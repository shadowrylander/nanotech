# Imports
import click

# From Imports
from loguru import logger
from sty import fg
from sys import exit
from typing import Dict, List, Any, Callable
from webcolors import hex_to_rgb

colors: Dict[str, Callable[..., Any]] = {
	"red": fg.red,
	"pink": fg(*hex_to_rgb("#f69")),
}
colorize = (
	lambda color, string: f"{colors[color]}{string}{fg.rs}"
)


class Error(Exception):
	pass


class illegal_usage(Error):
	pass


class nanite_op(click.Option):
	"""
		"xor"        : list of options this can't be used with
		"one_req"    : list of options of which one or more must be used
		"all_req"    : same as required
		"req_one_of" : list of options of which one or more must be used with this option
		"req_all_of" : list of options of which all must be used with this option

		Simply put the entire list of options in the keyword argument value; the help text and usage error will correct itself accordingly.
	"""

	def __init__(self, *args, **kwargs):
		self.xor: List[str] = kwargs.pop("xor", [])
		self.one_req: List[str] = kwargs.pop("one_req", [])
		self.req_one_of: List[str] = kwargs.pop("req_one_of", [])
		self.req_all_of: List[str] = kwargs.pop("req_all_of", [])
		help: str = kwargs.get("help", "")

		if "-" not in args[0] or "--" not in args[0]:
			name = args[0]
		elif "-" in args[0]:
			if "--" in args[1]:
				name = args[1].partition("--")[2]
			else:
				name = args[1]
		elif "--" in args[0]:
			name = args[0].partition("--")[2]

		if self.xor:
			self.xor_joined = (
				self.xor[0]
				if len(self.xor) == 1
				else '", "'.join(
					option
					for option in self.xor
					if option != name
				)
			)

			kwargs[
				"help"
			]: str = f'{help}\nNOTE: This option is mutually exclusive with {"option" if len(self.xor) == 1 else "options"} ["{self.xor_joined}"].'

			help: str = kwargs.get("help", "")

		if self.one_req:
			self.one_req_joined = (
				self.one_req[0]
				if len(self.one_req) == 1
				else '", "'.join(
					option
					for option in self.one_req
					if option != name
				)
			)

			kwargs[
				"help"
			]: str = f'{help}\nNOTE: This option must be used if {"option" if len(self.one_req) == 1 else "options"} ["{self.one_req_joined}"] {"is" if len(self.one_req) == 1 else "are"} not.'
			help: str = kwargs.get("help", "")

		if self.req_one_of:
			self.req_one_of_joined = (
				self.req_one_of[0]
				if len(self.req_one_of) == 1
				else '", "'.join(
					option
					for option in self.req_one_of
					if option != name
				)
			)

			kwargs[
				"help"
			]: str = f'{help}\nNOTE: This option requires {"the use" if len(self.req_one_of) == 1 else "one or more"} of {"option" if len(self.req_one_of) == 1 else "options"} ["{self.req_one_of_joined}"] as well.'

			help: str = kwargs.get("help", "")

		if self.req_all_of:
			self.req_all_of_joined = (
				self.req_all_of[0]
				if len(self.req_all_of) == 1
				else '", "'.join(
					option
					for option in self.req_all_of
					if option != name
				)
			)

			kwargs[
				"help"
			]: str = f'{help}\nNOTE: This option requires the use of {"option" if len(self.req_all_of) == 1 else "options"} ["{self.req_all_of_joined}"] as well.'

			help: str = kwargs.get("help", "")

		super(nanite_op, self).__init__(*args, **kwargs)

	def handle_parse_result(self, ctx, opts, args):
		"""

			Note to self: "self.name in opts" was being used because if it wasn't,
			the if condition would match regardless of whether this option is being used or not;
			so for example, if option "a" is mutually exclusive to option "b", and
			"self.name in opts" wasn't used, "command -a -b" would match the condition, but so would
			"command -a" and "command -b", given that the option is still being parsed by
			the program.
		"""

		if (
			self.name in opts
			and self.xor
			and any(option in opts for option in self.xor)
		):
			logger.error(
				colorize("red", "Sorry, something happened!\n\n")
			)
			raise illegal_usage(
				f'Illegal Usage: "{self.name}" is mutually exclusive with {"option" if len(self.xor) == 1 else "options"}:\n\n["{self.xor_joined}"]'
			)

		if (
			self.one_req
			and self.name not in opts
			and not any(
				option in opts for option in self.one_req
			)
		):
			logger.error(
				colorize("red", "Sorry, something happened!\n\n")
			)
			raise illegal_usage(
				f'Illegal Usage: one of these options are required:\n\n["{self.one_req_joined}"]'
			)

		if (
			self.name in opts
			and self.req_one_of
			and not any(
				option in opts for option in self.req_one_of
			)
		):
			logger.error(
				colorize("red", "Sorry, something happened!\n\n")
			)
			raise illegal_usage(
				f'Illegal Usage: "{self.name}" must be used with {"" if len(self.req_one_of) == 1 else "one or more of"} {"option" if len(self.req_one_of) == 1 else "options"}:\n\n["{self.req_one_of_joined}"]'
			)

		if (
			self.name in opts
			and self.req_all_of
			and not all(
				option in opts for option in self.req_all_of
			)
		):
			logger.error(
				colorize("red", "Sorry, something happened!\n\n")
			)
			raise illegal_usage(
				f'Illegal Usage: "{self.name}" must be used with {"option" if len(self.req_all_of) == 1 else "options"}:\n\n["{self.req_all_of_joined}"]'
			)

		return super(nanite_op, self).handle_parse_result(
			ctx, opts, args
		)


# class nanite_opString(click.Option):
#     """
# 		Credits for MutuallyExclusiveOption: https://gist.github.com/jacobtolar/fb80d5552a9a9dfc32b12a829fa21c0c
#
# 		Credits for GroupedOptions: https://stackoverflow.com/questions/55874075/use-several-options-together-or-not-at-all/55881912#55881912
# 		Answer By: https://stackoverflow.com/users/7311767/stephen-rauch
#
# 		Modified by Jeet Ray <titaniumfiles@outlook.com>
#
# 		# Update for the one_of and requires usage
# 		Usage:
# 		@click.option(
# 			"-t-1",
# 			"--test-1",
# 			is_flag=True,
# 			help="Test 1 Flag",
# 			cls=nanite_op,
# 			xor=["t_2", "t_4"],
# 			group="odd",
# 		)
# 		@click.option(
# 			"-t-3",
# 			"--test-3",
# 			is_flag=True,
# 			help="Test 3 Flag",
# 			cls=nanite_op,
# 			xor=["t_2", "t_4"],
# 			group="odd",
# 		)
# 		@click.option(
# 			"-t-2",
# 			"--test-2",
# 			is_flag=True,
# 			help="Test 2 Flag",
# 			cls=nanite_op,
# 			xor=["t_1", "t_3"],
# 			group="even",
# 		)
# 		@click.option(
# 			"-t-4",
# 			"--test-4",
# 			is_flag=True,
# 			help="Test 4 Flag",
# 			cls=nanite_op,
# 			xor=["t_1", "t_3"],
# 			group="even",
# 		)
#
#         "requires" should be used when one option requires one or more options together, but not vice-versa.
#         "req_one_of" should be used when one option requires one or more options optionally, but not vice-versa.
#         "one_of" should be used when one or more options in the group are required.
#         "group" should be used when all options are required.
# 	"""
#
#     def __init__(self, *args, **kwargs):
#         # self.xor: str = kwargs.pop("xor", "")
#         self.group: str = kwargs.pop("group", "")
#         self.one_req: str = kwargs.pop("one_req", "")
#         # self.req_one_of: str = kwargs.pop("req_one_of", "")
#         # self.req_all_of: str = kwargs.pop("req_all_of", "")
#         super(nanite_opString, self).__init__(*args, **kwargs)
#
#     def handle_parse_result(self, ctx, opts, args):
#         """
#
# 			Note to self: "self.name in opts" is being used because if it isn't,
# 			the if condition would match regardless of whether this option is being used or not;
# 			so for example, if option "a" is mutually exclusive to option "b", and
# 			"self.name in opts" wasn't used, "command -a -b" would match the condition, but so would
# 			"command -a" and "command -b", given that the option is still being parsed by
# 			the program.
# 		"""
#         if self.name in opts and self.group:
#             opts_in_group = [
#                 param.name
#                 for param in ctx.command.params
#                 if isinstance(param, nanite_opString)
#                 and param.group == self.group
#             ]
#
#             missing_specified = any(
#                 option
#                 for option in opts_in_group
#                 if option not in opts
#             )
#
#             if missing_specified:
#                 opts_in_group_joined = '", "'.join(
#                     option
#                     for option in opts_in_group
#                     if option != self.name
#                 )
# 				  logger.error(
# 				  	  colorize(
# 						  "red",
# 						  'Sorry, something happened!\n\n',
# 					  )
# 				  )
#                 raise illegal_usage(f'Illegal Usage: "{self.name}" must be used with {"option" if len(opts_in_group) == 1 else "options"}:\n\n["{opts_in_group_joined}"]')
#
#         if self.name in opts and self.one_req:
#             pass
#         elif self.name not in opts and self.one_req:
#             one_req_group: List[str] = [
#                 param.name
#                 for param in ctx.command.params
#                 if isinstance(param, nanite_opString)
#                 and param.one_req == self.one_req
#             ]
#
#             """
#                 Note to self; Understand as:
#
#                 lst = []
#                 for option in one_req_group:
#                     if option in opts:
#                         lst.append(option)
#                 one_exists = any(lst)
#
#                 OR:
#
#                 lst = []
#                 for option in one_req_group:
#                     lst.append(option in opts)
#                 one_exists = any(lst)
#             """
#             one_exists = any(
#                 option in opts for option in one_req_group
#             )
#
#             if not one_exists:
#                 one_req_group_joined = '", "'.join(one_req_group)
# 				  logger.error(
# 				  	  colorize(
# 						  "red",
# 						  'Sorry, something happened!\n\n',
# 					  )
# 				  )
#                 raise illegal_usage(f'Illegal Usage; you must use one or more of options:\n\n["{one_req_group_joined}"]')
#
#         return super(nanite_opString, self).handle_parse_result(
#             ctx, opts, args
#         )


class nanite_comm(click.Command):
	"""
		Credits for DefaultHelp: https://stackoverflow.com/questions/50442401/how-to-set-the-default-option-as-h-for-python-click/50491613#50491613
		Answer By: https://stackoverflow.com/users/7311767/stephen-rauch

		Modified by Jeet Ray <titaniumfiles@outlook.com>

	"""

	# Modify to pass in a pass option if any subcommands are passed in
	def __init__(self, *args, **kwargs):
		context_settings = kwargs.setdefault(
			"context_settings", {}
		)
		if "help_option_names" not in context_settings:
			context_settings["help_option_names"] = [
				"-h",
				"--help",
			]
		self.help_flag = context_settings["help_option_names"][0]
		self.no_args_is_help: bool = kwargs.pop(
			"no_args_is_help", False
		)
		super(nanite_comm, self).__init__(*args, **kwargs)

	def parse_args(self, ctx, args):
		if self.no_args_is_help and not any(args):
			args = [self.help_flag]
		return super(nanite_comm, self).parse_args(ctx, args)

