# From Imports
from os import path as os_path
from typing import List
from nanite import fullpath, module_installed

# Debug Imports
pout = module_installed("pout")

"""
	Variables "glob_list" and "regex_list" take an iterable
	(list, tuple, generator, etc.) of exclusion paths in
	the "glob" and "regex" formats respectively.

	In addition, all paths with environment variables or tildes
	will be expanded as well.
"""

# Creates a ".hgignore" file
def hg(
	source: str,
	glob_list: List[str] = None,
	regex_list: List[str] = None,
) -> None:
	"""
		Create and populate the "mercurial" ignore file
	"""
	ignore_file: str = fullpath(source, ".hgignore")
	if os_path.isfile(ignore_file):
		print("Using existing file!")
	else:
		# Use the context manager from the O'Reilly article here...?
		with open(ignore_file, "w") as ignore:
			if glob_list:
				ignore.write("syntax: glob\n\n")
				for excl in glob_list:
					ignore.write(f"{excl}\n")
				ignore.write("\n")
			if regex_list:
				ignore.write("syntax: regexp\n\n")
				for excl in regex_list:
					ignore.write(f"{excl}\n")
				ignore.write("\n")


# Creates a ".gitignore" file
def git(source: str, fm_list: List[str] = None) -> None:
	"""
		Create and populate the "git" ignore file
	"""
	ignore_file: str = fullpath(source, ".gitignore")
	if os_path.isfile(ignore_file):
		print("Using existing file!")
	else:
		if fm_list:
			# Use the context manager from the O'Reilly article here...?
			with open(ignore_file, "w") as ignore:
				for excl in fm_list:
					ignore.write(f"{excl}\n")


# Creates an fd ignore file
def fd(ignore_file: str, glob_list: List[str] = None) -> None:
	"""
		Create and populate the "fd" ignore file
	"""
	with open(fullpath(ignore_file), "w") as ignore:
		if glob_list:
			for excl in glob_list:
				ignore.write(f"{excl}\n")


# def glob(glob_list: List[str]) -> str:
#     """
#         Create the necessary bracketed glob pattern for the glob module
#     """
#     return "[!" + "][!".join(glob_list) + "]"


# Creates a borg ignore file
def borg(
	ignore_file: str,
	glob_list: List[str] = None,
	regex_list: List[str] = None,
	fm_list: List[str] = None,
	pp_list: List[str] = None,
	pf_list: List[str] = None,
) -> None:
	"""
		Create and populate the "borg" ignore file
	"""
	with open(fullpath(ignore_file), "w") as ignore:
		if glob_list:
			for excl in glob_list:
				ignore.write(f"sh:{excl}\n")
		if regex_list:
			for excl in regex_list:
				ignore.write(f"re:{excl}\n")
		if fm_list:
			for excl in fm_list:
				ignore.write(f"fm:{excl}\n")
		if pp_list:
			for excl in pp_list:
				ignore.write(f"pp:{excl}\n")
		if pf_list:
			for excl in pf_list:
				ignore.write(f"pf:{excl}\n")
